-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: finalProjectdb
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cartitemstable`
--

DROP TABLE IF EXISTS `cartitemstable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cartitemstable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quantity` int DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `cart_id` bigint DEFAULT NULL,
  `item_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKq3vai08lkbhc6m6jah0h3gp10` (`cart_id`),
  KEY `FKbajieip3c81jqg3wh9gasnjcj` (`item_id`),
  CONSTRAINT `FKbajieip3c81jqg3wh9gasnjcj` FOREIGN KEY (`item_id`) REFERENCES `menutable` (`id`),
  CONSTRAINT `FKq3vai08lkbhc6m6jah0h3gp10` FOREIGN KEY (`cart_id`) REFERENCES `cartstable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartitemstable`
--

LOCK TABLES `cartitemstable` WRITE;
/*!40000 ALTER TABLE `cartitemstable` DISABLE KEYS */;
/*!40000 ALTER TABLE `cartitemstable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cartstable`
--

DROP TABLE IF EXISTS `cartstable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cartstable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quantity` int DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `customer_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKoohlxdp3kynoyqf186n39f2a7` (`customer_id`),
  CONSTRAINT `FKoohlxdp3kynoyqf186n39f2a7` FOREIGN KEY (`customer_id`) REFERENCES `customertable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartstable`
--

LOCK TABLES `cartstable` WRITE;
/*!40000 ALTER TABLE `cartstable` DISABLE KEYS */;
INSERT INTO `cartstable` VALUES (1,0,0,1),(2,0,0,2),(3,1,307.4,3),(4,2,312.70000000000005,4),(5,0,0,5),(6,0,0,6),(7,0,0,8);
/*!40000 ALTER TABLE `cartstable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customeraddress`
--

DROP TABLE IF EXISTS `customeraddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customeraddress` (
  `customer_id` bigint NOT NULL,
  `adress_line1` varchar(50) DEFAULT NULL,
  `address_line2` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `pincode` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  KEY `FK49loblvnt0271q7hor5i1bbl1` (`customer_id`),
  CONSTRAINT `FK49loblvnt0271q7hor5i1bbl1` FOREIGN KEY (`customer_id`) REFERENCES `customertable` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customeraddress`
--

LOCK TABLES `customeraddress` WRITE;
/*!40000 ALTER TABLE `customeraddress` DISABLE KEYS */;
INSERT INTO `customeraddress` VALUES (1,'At Post Hadpsar','PUNE','Nagar','India','414001','Maharashtra'),(2,'At Post kej','Beed','Pune','India','414103','Maharashtra'),(3,'At Post Ratnagiri','Ratnagiri','Pune','India','414106','Maharashtra'),(5,'AT POST HIWARE BAZAR,TAL-NAGAR,DIST-AHMEDNAGAR','Ahmednagar','Nagar','India','414103','Maharashtra'),(6,'AT POST HIWARE BAZAR,TAL-NAGAR,DIST-AHMEDNAGAR','Ahmednagar','Nagar','India','414103','Maharashtra'),(8,'Pune','Pune','Nagar','India','414102','Maharashtra'),(4,'At Post Sangali','Sangali','Nagar','India','414105','Maharashtra'),(4,'AT POST HIWARE BAZAR,TAL-NAGAR,DIST-AHMEDNAGAR','pune','Pune','India','414103','Maharashtra');
/*!40000 ALTER TABLE `customeraddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customertable`
--

DROP TABLE IF EXISTS `customertable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customertable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `gender` int DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `mobile_no` varchar(10) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_n6vwq4nqoadfpmp7tkovc9jo8` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customertable`
--

LOCK TABLES `customertable` WRITE;
/*!40000 ALTER TABLE `customertable` DISABLE KEYS */;
INSERT INTO `customertable` VALUES (1,'rishi@gmail.com','Rishi',0,'Shinde','8459435675','1234','Rishi123.jpg','Rishi123'),(2,'sharad@gmail.com','Sharad',0,'Hange','7588234565','1234','Sharad123.jpg','Sharad123'),(3,'omkar@gmail.com','Omkar',0,'Patil','9876543212','1234','Omkar123.jpg','Omkar123'),(4,'krishna@gmail.com','Krishna',0,'Mokashi','8765432345','1234','Krishna123.jpg','Krishna123'),(5,'pratik@gmail.com','Pratik',0,'Patil','8654324578','1234','Pratik123.jpg','Pratik123'),(6,'sagar@gmail.com','Sagar',0,'Patil','8676543245','1234','Sagar123.jpg','Sagar123'),(8,'shubhass@gmail.com','Shubhass',0,'Patil','8765432345','1234','Shubhass123.jpg','Shubhass123');
/*!40000 ALTER TABLE `customertable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debitcards`
--

DROP TABLE IF EXISTS `debitcards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `debitcards` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `card_holder_name` varchar(50) DEFAULT NULL,
  `card_number` varchar(50) DEFAULT NULL,
  `cvv` varchar(50) DEFAULT NULL,
  `expdate` date DEFAULT NULL,
  `customer_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_27r1mvhxt7816m2ciyqjw4bkw` (`card_number`),
  KEY `FK13xqcbsw92nlqy9qo75lmmu0p` (`customer_id`),
  CONSTRAINT `FK13xqcbsw92nlqy9qo75lmmu0p` FOREIGN KEY (`customer_id`) REFERENCES `customertable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debitcards`
--

LOCK TABLES `debitcards` WRITE;
/*!40000 ALTER TABLE `debitcards` DISABLE KEYS */;
INSERT INTO `debitcards` VALUES (1,'Rishi Shinde','3456321276544567','765','2032-05-08',1),(2,'Sharad Hange','9876543212348908','876','2028-09-11',2),(3,'Omkar Patil','8976543234568909','543','2023-03-21',3),(4,'Krishna Mokashi','6543789032456789','543','2023-03-28',4),(5,'Pratik Patil','8754323456789645','765','2027-10-21',5),(19,'Sagar Patil','345675432','654','2023-03-16',6),(20,'Shubhass','7654345678907654','654','2023-03-18',8);
/*!40000 ALTER TABLE `debitcards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliveryboytable`
--

DROP TABLE IF EXISTS `deliveryboytable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `deliveryboytable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `adress_line1` varchar(50) DEFAULT NULL,
  `address_line2` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `pincode` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `delivery_boy_status` int DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `gender` int DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `mobile_no` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `order_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcijyfic2lqbmvmked4wtjncim` (`order_id`),
  CONSTRAINT `FKcijyfic2lqbmvmked4wtjncim` FOREIGN KEY (`order_id`) REFERENCES `orderstable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveryboytable`
--

LOCK TABLES `deliveryboytable` WRITE;
/*!40000 ALTER TABLE `deliveryboytable` DISABLE KEYS */;
INSERT INTO `deliveryboytable` VALUES (1,'At Post Pune','Pune','Pune','India','414104','Maharashtra',1,'shrini@gmail.com','Shrini',0,'Patil','9876543234','1234','shrini@gmail.com.jpg','Shrini123',NULL);
/*!40000 ALTER TABLE `deliveryboytable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentstable`
--

DROP TABLE IF EXISTS `documentstable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `documentstable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `aadhar_no` varchar(12) DEFAULT NULL,
  `aadhar_photo` varchar(255) DEFAULT NULL,
  `fsaii_no` varchar(14) DEFAULT NULL,
  `fsaii_photo` varchar(255) DEFAULT NULL,
  `hotelphoto` varchar(255) DEFAULT NULL,
  `pan_no` varchar(50) DEFAULT NULL,
  `pan_photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentstable`
--

LOCK TABLES `documentstable` WRITE;
/*!40000 ALTER TABLE `documentstable` DISABLE KEYS */;
INSERT INTO `documentstable` VALUES (1,'A1235','aadharVaibhav123.jpg','F12345','fsaiiVaibhav123.jpg','hotelPhotoVaibhav123.jpg','P12345','panVaibhav123.jpg'),(2,'A243','aadharAniket123.jpg','F456','fsaiiAniket123.jpg','hotelPhotoAniket123.jpg','P323','panAniket123.jpg'),(3,'A1234','aadharSagar123.jpg','F123','fsaiiSagar123.jpg','hotelPhotoSagar123.jpg','P123','panSagar123.jpg');
/*!40000 ALTER TABLE `documentstable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelmanagertable`
--

DROP TABLE IF EXISTS `hotelmanagertable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotelmanagertable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `adress_line1` varchar(50) DEFAULT NULL,
  `address_line2` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `pincode` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `hotelname` varchar(50) DEFAULT NULL,
  `hotel_type` int DEFAULT NULL,
  `hotel_status` int DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `mobile_no` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `documents_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ob6et704fakh578qw8hsyyjlj` (`email`),
  KEY `FKntc4e8k9p5eos9smdlv6n75k7` (`documents_id`),
  CONSTRAINT `FKntc4e8k9p5eos9smdlv6n75k7` FOREIGN KEY (`documents_id`) REFERENCES `documentstable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelmanagertable`
--

LOCK TABLES `hotelmanagertable` WRITE;
/*!40000 ALTER TABLE `hotelmanagertable` DISABLE KEYS */;
INSERT INTO `hotelmanagertable` VALUES (1,'Senapati Bapat Road','Pune','Pune','India','414001','Maharashtra','Great Veg Options, Best Food under Best Budget, Tamper Proof Packaging, Fresh Food with Good Quality, Mouth Watering Food, Affordable Prices@ good@ Good','vaibhav@gmail.com','Vaibhav','Red Chinese And Momos',0,1,'Patil','8459968355','1234',3,'Vaibhav123',1),(2,'Phase 1 ,Hinjewadi','Pune','Pune','India','414106','Maharashtra','Restaurants in Pune, Pune Restaurants, Shaniwar Peth restaurants, Best Shaniwar Peth restaurants, Deccan & Peths restaurants, Quick Bites in Pune, Quick Bites near me','aniket@gmail.com','Aniket','McDonald\'s',1,1,'Shinde','8765432123','1234',0,'Aniket123',2),(3,'AT POST HIWARE BAZAR,TAL-NAGAR,DIST-AHMEDNAGAR','Mumbai','Nashik','India','414103','Maharashtra','Good Hotel','sagar@gmail.com','Sagar','SagarHotel',1,0,'Patil','8459968355','1234',0,'Sagar123',3);
/*!40000 ALTER TABLE `hotelmanagertable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menutable`
--

DROP TABLE IF EXISTS `menutable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menutable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `cusine_category` int DEFAULT NULL,
  `dish_name` varchar(50) DEFAULT NULL,
  `dish_photo` varchar(50) DEFAULT NULL,
  `mill_type` int DEFAULT NULL,
  `price` int DEFAULT NULL,
  `size` int DEFAULT NULL,
  `veg_non_veg_category` int DEFAULT NULL,
  `hotel_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKek1vvxq32ld4m4jj5w9xtt5nc` (`hotel_id`),
  CONSTRAINT `FKek1vvxq32ld4m4jj5w9xtt5nc` FOREIGN KEY (`hotel_id`) REFERENCES `hotelmanagertable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menutable`
--

LOCK TABLES `menutable` WRITE;
/*!40000 ALTER TABLE `menutable` DISABLE KEYS */;
INSERT INTO `menutable` VALUES (2,0,'Veg Garlic Rice','Veg Garlic RiceVaibhav123.jpg',2,119,2,0,1),(3,1,'Chicken Fried Rice ','Chicken Fried Rice Vaibhav123.jpg',2,176,3,1,1),(4,1,'Chicken LolyPopFry','Chicken LolyPopFryVaibhav123.jpg',3,161,2,1,1),(5,1,'Chicken Shezwan Nuddles','Chicken Shezwan NuddlesVaibhav123.jpg',2,345,3,1,1),(6,0,'Corn & Cheese Burger','Corn & Cheese BurgerAniket123.jpg',2,145,1,0,2),(7,0,'McVeggieBurger','McVeggieBurgerAniket123.jpg',3,145,1,0,2),(8,0,'McCheese Burger','McCheese BurgerAniket123.jpg',2,189,1,0,2),(9,0,'2 McVeggie Burger','2 McVeggie BurgerAniket123.jpg',3,200,1,0,2),(10,0,'Dish1','Dish1Sagar123.jpg',2,123,1,0,3);
/*!40000 ALTER TABLE `menutable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderdetailtable`
--

DROP TABLE IF EXISTS `orderdetailtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orderdetailtable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quantity` int DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `dish_id` bigint DEFAULT NULL,
  `order_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfe8qh34h2gjr52vya3iy8xs5g` (`dish_id`),
  KEY `FKh254sdilo1xh3iourabwsrguk` (`order_id`),
  CONSTRAINT `FKfe8qh34h2gjr52vya3iy8xs5g` FOREIGN KEY (`dish_id`) REFERENCES `menutable` (`id`),
  CONSTRAINT `FKh254sdilo1xh3iourabwsrguk` FOREIGN KEY (`order_id`) REFERENCES `orderstable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderdetailtable`
--

LOCK TABLES `orderdetailtable` WRITE;
/*!40000 ALTER TABLE `orderdetailtable` DISABLE KEYS */;
INSERT INTO `orderdetailtable` VALUES (1,2,145,7,1),(2,1,145,6,2),(3,1,145,7,2),(4,3,145,6,3),(5,1,176,3,4),(6,2,119,2,4),(7,1,119,2,5),(8,1,176,3,5);
/*!40000 ALTER TABLE `orderdetailtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderstable`
--

DROP TABLE IF EXISTS `orderstable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orderstable` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `cooking_status` int DEFAULT NULL,
  `delivery_status` int DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `order_status` int DEFAULT NULL,
  `price` double DEFAULT NULL,
  `customer_id` bigint DEFAULT NULL,
  `deliveryboy_id` bigint DEFAULT NULL,
  `hotel_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqn0dqr04hfg8grjhmh9mpopx2` (`customer_id`),
  KEY `FK8fchos85jylg53fhlylopsef8` (`deliveryboy_id`),
  KEY `FK2wu7bp51hvsi0t6gm73bucujh` (`hotel_id`),
  CONSTRAINT `FK2wu7bp51hvsi0t6gm73bucujh` FOREIGN KEY (`hotel_id`) REFERENCES `hotelmanagertable` (`id`),
  CONSTRAINT `FK8fchos85jylg53fhlylopsef8` FOREIGN KEY (`deliveryboy_id`) REFERENCES `deliveryboytable` (`id`),
  CONSTRAINT `FKqn0dqr04hfg8grjhmh9mpopx2` FOREIGN KEY (`customer_id`) REFERENCES `customertable` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderstable`
--

LOCK TABLES `orderstable` WRITE;
/*!40000 ALTER TABLE `orderstable` DISABLE KEYS */;
INSERT INTO `orderstable` VALUES (1,0,2,'2023-03-09',0,307.4,3,NULL,2),(2,3,1,'2023-03-09',0,307.4,4,1,2),(3,3,2,'2023-03-09',0,461.09999999999997,4,NULL,2),(4,3,1,'2023-03-09',0,438.84,4,1,1),(5,3,1,'2023-03-09',0,312.70000000000005,4,1,1);
/*!40000 ALTER TABLE `orderstable` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-09 19:59:47
